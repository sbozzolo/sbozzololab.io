+++
title = "Post 0: Where the writer introduces his plans"
description = "I don't believe in humans, I believe in systems. - **CGPGray**"
date = 2019-02-11T00:00:00-07:00
draft = false
creator = "Emacs 26.1 (Org mode 9.2.1 + ox-hugo)"
+++

> I don't believe in humans, I believe in systems.
>
> **CGPGray**

This blog is not for you, the reader. I am writing this post, and all the
ones that will come in the future, because I want to introduce some
modifications to my life. I feel that I need to
